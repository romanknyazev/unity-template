﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Hoba
{
    public class AutoRotate : MonoBehaviour
    {
        public Vector3 axis = new Vector3(0, 1, 0);
        public float speed = 10.0f;
        
        private void FixedUpdate()
        {
            float a = Time.fixedDeltaTime * speed;
            transform.Rotate(axis, a, Space.Self);
        }
    }
}


